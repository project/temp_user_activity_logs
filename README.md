Readme
================================================================================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
================================================================================
This module is used to keep track of all pages that authenticated
users are visiting. It keeps temporary logs in Database of all
paths with some additional details.

Features Include:

    1.  Log all paths users are visiting
    2.  Temporary storage of logs (Maximum 30 days)
    3.  Automatically delete logs using cron


REQUIREMENTS
================================================================================
Drupal 7.x


INSTALLATION
================================================================================
admin/config/system/temp_user_activity_logs


CONFIGURATION
================================================================================
Make sure that cron jobs are scheduled. This module uses hook_cron()
to automatically delete old logs from the database.

After installing this module keep track of your Database size (from daily 
backups).
If cron is disabled the Database size will keep growing.
